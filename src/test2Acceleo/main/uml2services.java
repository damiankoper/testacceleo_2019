package test2Acceleo.main;
 
 import java.util.HashSet;
import java.util.Set;

import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.MergeNode;

 public class uml2services {
	 
	public Set<MergeNode> getMergeNodes(ActivityNode sourceNode) {
		 Set<MergeNode> mergeNodes = new HashSet<MergeNode>();
		 
		 for(ActivityEdge outg: sourceNode.getOutgoings()) {
			 ActivityNode target = outg.getTarget();
			 if(target instanceof MergeNode) {
				 mergeNodes.add((MergeNode)target);
			 } else {
				 mergeNodes.addAll(getMergeNodes(target));
			 }
		 }
		 
		 return mergeNodes;
	 }
 }